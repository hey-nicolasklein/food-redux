import React from 'react';
import { connect } from 'react-redux';
import { getFood } from '../redux/selectors.js';
import { Food } from './Food.js';

function FoodList(props){ 
  return(
    <div>
      <h1>FoodList</h1>
      <ul>
        {
          props.food ? 
          props.food.map((food, idx) => (<Food key={`food-${idx}`} food={food}/>))
          : "Nothing to display"
        }
      </ul>
    </div>
  )
}

export default connect(state => ({food: getFood(state)}))(FoodList);

