import React from 'react';
import { useState } from "react";
import { connect } from "react-redux";
import { addFood } from "../redux/actions"

const AddFood = ({addFood}) => {
  
  const [input, changeInput] = useState('');


  function handleAdd(){
    addFood(input);
    changeInput("");
  }

  return (<div>
    <input 
      onChange={(e) => changeInput(e.target.value)}
      value={input}
    />
    <button onClick={handleAdd}>
      Add to list
    </button>
  </div>)
}

export default connect(null, {addFood})(AddFood);