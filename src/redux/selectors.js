export const getFoodList = store => store.food ? store.food : [];

export const getFood = store => getFoodList(store);