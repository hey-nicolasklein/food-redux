const initalState = {
  food: []
}

export default function (state = initalState, action) {
  switch(action.type){
    case "ADD_FOOD":
      return {...state, food: [...state.food, {name: action.payload.content}]}
    case "DELETE_FOOD":
      return state; //TODO
    default:
      return state;
  }
}