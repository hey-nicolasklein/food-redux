export const addFood = content => ({
  type: "ADD_FOOD",
  payload: {
    content
  }
});