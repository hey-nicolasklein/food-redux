import React from 'react';
import AddFood from './components/AddFood';
import FoodList from './components/FoodList';

function App() {
  return (
    <div className="App">
      <AddFood/>
      <FoodList/>
    </div>
  );
}

export default App;
